package com.fraserkillip.mods.hexis;

import java.io.File;

import net.minecraftforge.common.config.Configuration;

import com.fraserkillip.mods.hexis.blocks.BlockInfo;
import com.fraserkillip.mods.hexis.items.ItemInfo;

public class ConfigHandler {

	public static void init(File file) {
		Configuration config = new Configuration(file);
		
		config.load();
		// --- Blocks
//		BlockInfo.SOLAR_FURNACE_ID = config.getBlock(BlockInfo.SOLAR_FURNACE_UNLOCALIZED, BlockInfo.SOLAR_FURNACE_DEFAULT).getInt();
//		BlockInfo.MIRROR_ID = config.getBlock(BlockInfo.MIRROR_KEY, BlockInfo.MIRROR_DEFAULT).getInt();
//		
//		// --- Items
//		ItemInfo.CLIPBOARD_ID = config.getItem(ItemInfo.CLIPBOARD_UNLOCALIZED, ItemInfo.CLIPBOARD_DEFAULT_ID).getInt() - 256;
//		
//		ItemInfo.GRAVISTAFF_ID = config.getItem(ItemInfo.GRAVISTAFF_UNLOCALIZED, ItemInfo.GRAVISTAFF_DEFAULT_ID).getInt() - 256;
//		ItemInfo.GRAVISTAFF_COOLDOWN = config.get("Configurables", ItemInfo.GRAVISTAFF_COOLDOWN_KEY, ItemInfo.GRAVISTAFF_COOLDOWN_DEFAULT, "Number of seconds between uses").getInt();
//		
//		ItemInfo.CORUNDUM_RAW_ID = config.getItem(ItemInfo.CORUNDUM_RAW_UNLOCALIZED, ItemInfo.CORUNDUM_RAW_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_MONO_ID = config.getItem(ItemInfo.CORUNDUM_MONO_UNLOCALIZED, ItemInfo.CORUNDUM_MONO_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_PURE_ID = config.getItem(ItemInfo.CORUNDUM_PURE_UNLOCALIZED, ItemInfo.CORUNDUM_PURE_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_TI_ID = config.getItem(ItemInfo.CORUNDUM_TI_UNLOCALIZED, ItemInfo.CORUNDUM_TI_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_TI_BLUE_ID = config.getItem(ItemInfo.CORUNDUM_TI_BLUE_UNLOCALIZED, ItemInfo.CORUNDUM_TI_BLUE_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_BLUE_ID = config.getItem(ItemInfo.CORUNDUM_BLUE_UNLOCALIZED, ItemInfo.CORUNDUM_BLUE_DEFAULT_ID).getInt() - 256;
//		ItemInfo.CORUNDUM_RED_ID = config.getItem(ItemInfo.CORUNDUM_RED_UNLOCALIZED, ItemInfo.CORUNDUM_RED_DEFAULT_ID).getInt() - 256;
//		
//		ItemInfo.LENS_CONVERGING_ID = config.getItem(ItemInfo.LENS_CONVERGING_UNLOCALIZED, ItemInfo.LENS_CONVERGING_DEFAULT_ID).getInt() - 256;
//		ItemInfo.LENS_DIVERGING_ID = config.getItem(ItemInfo.LENS_DIVERGING_UNLOCALIZED, ItemInfo.LENS_DIVERGING_DEFAULT_ID).getInt() - 256;
//		ItemInfo.LENS_MOUNT_ID = config.getItem(ItemInfo.LENS_MOUNT_UNLOCALIZED, ItemInfo.LENS_MOUNT_DEFAULT_ID).getInt() - 256;
//		ItemInfo.LENS_GUIDE_ID = config.getItem(ItemInfo.LENS_GUIDE_UNLOCALIZED, ItemInfo.LENS_GUIDE_DEFAULT_ID).getInt() - 256;
//		ItemInfo.LENS_FOCUS_ID = config.getItem(ItemInfo.LENS_FOCUS_UNLOCALIZED, ItemInfo.LENS_FOCUS_DEFAULT_ID).getInt() - 256;
//		ItemInfo.LENS_SIZER_ID = config.getItem(ItemInfo.LENS_SIZER_UNLOCALIZED, ItemInfo.LENS_SIZER_DEFAULT_ID).getInt() - 256;
		
		config.save();
	}
}
