package com.fraserkillip.mods.hexis;

import net.minecraft.creativetab.CreativeTabs;

import com.fraserkillip.mods.hexis.blocks.HexisBlocks;
import com.fraserkillip.mods.hexis.client.CreativeTabHexis;
import com.fraserkillip.mods.hexis.items.HexisItems;
import com.fraserkillip.mods.hexis.proxies.CommonProxy;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = ModInformation.MOD_NAME, name = ModInformation.MOD_NAME, version = ModInformation.MOD_VERSION)
public class Hexis {
	
	@Instance
	public static Hexis instance;
	
	public static CreativeTabs creativeTab;
	
	@SidedProxy(clientSide = "com.fraserkillip.mods.hexis.proxies.ClientProxy", serverSide = "com.fraserkillip.mods.hexis.proxies.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Set up creative tab
		creativeTab = new CreativeTabHexis();
		
		
		ConfigHandler.init(event.getSuggestedConfigurationFile());
		HexisItems.init();
		HexisBlocks.init();
				
		proxy.initRenderers();
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event) {
		HexisItems.registerRecipes();
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
}
