package com.fraserkillip.mods.hexis;

public class ModInformation {
	
	public static final String MOD_ID = "Hexis";
	public static final String MOD_NAME = "Hexis";
	public static final String MOD_VERSION = "0.0.1";
	
	public static final String MOD_CHANNEL = "Hexis";
	
	public static final String TEXTURE_LOC = "hexis";
	
}
