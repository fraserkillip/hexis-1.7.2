package com.fraserkillip.mods.hexis.blocks;

public class BlockInfo {

	public static final String SOLAR_FURNACE_KEY = "solarFurnace";
	public static final String SOLAR_FURNACE_UNLOCALIZED = "hexis.solarFurnace";
	
	public static final String MIRROR_KEY = "mirror";
	public static final String MIRROR_UNLOCALIZED = "hexis.mirror";
	public static final String MIRROR_ICON = "mirror_block";

}
