package com.fraserkillip.mods.hexis.blocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import com.fraserkillip.mods.hexis.Hexis;
import com.fraserkillip.mods.hexis.ModInformation;
import com.fraserkillip.mods.hexis.items.ItemInfo;
import com.fraserkillip.mods.hexis.tileentities.TileEntityMirror;

public class BlockMirror extends BlockContainer {

	public BlockMirror() {
		super(Material.rock);
		setBlockName(BlockInfo.MIRROR_UNLOCALIZED);

		setHardness(1F);
		setStepSound(soundTypeMetal);
		setCreativeTab(Hexis.creativeTab);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int var2) {
		return new TileEntityMirror();
	}
	
	@Override
	public String getItemIconName() {
		// TODO Auto-generated method stub
		return ModInformation.TEXTURE_LOC + ":" + BlockInfo.MIRROR_ICON;
	}
	
	@Override
	public boolean renderAsNormalBlock() {
		return false;
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
	
	@Override
	public int getRenderType() {
		return -1;
	}

}
