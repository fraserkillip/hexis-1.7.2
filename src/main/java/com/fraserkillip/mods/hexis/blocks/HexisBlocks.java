package com.fraserkillip.mods.hexis.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;

public class HexisBlocks {

	public static Block solarFurnace;
	public static Block	mirror;
	
	public static void init() {
		solarFurnace = new BlockSolarFurnace();
		GameRegistry.registerBlock(solarFurnace, BlockInfo.SOLAR_FURNACE_KEY);
		
		mirror  = new BlockMirror();
		GameRegistry.registerBlock(mirror, BlockInfo.MIRROR_KEY);
	}
}
