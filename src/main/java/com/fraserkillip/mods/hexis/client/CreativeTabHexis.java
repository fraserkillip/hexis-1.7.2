package com.fraserkillip.mods.hexis.client;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

import com.fraserkillip.mods.hexis.ModInformation;
import com.fraserkillip.mods.hexis.items.HexisItems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class CreativeTabHexis extends CreativeTabs {

	public CreativeTabHexis() {
		super(ModInformation.MOD_ID);
		// TODO Auto-generated constructor stub
	}

	@Override
	@SideOnly(Side.CLIENT)
	public Item getTabIconItem() {
		return HexisItems.corundumTiBlue;
	}

}
