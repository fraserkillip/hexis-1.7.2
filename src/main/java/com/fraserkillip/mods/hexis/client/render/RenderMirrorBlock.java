package com.fraserkillip.mods.hexis.client.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;

import org.lwjgl.opengl.GL11;

import com.fraserkillip.mods.hexis.tileentities.TileEntityMirror;

public class RenderMirrorBlock extends TileEntitySpecialRenderer {

	private IModelCustom model;

	public RenderMirrorBlock(IModelCustom model) {
		this.model = model;
	}

	public static final ResourceLocation texture = new ResourceLocation("hexis", "textures/models/mirror_block.png");

	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f) {
		GL11.glPushMatrix();

		GL11.glTranslatef((float) x + 0.5F, (float) y, (float) z + 0.5F);
		GL11.glScalef(1.15F, 1.15F, 1.15F);

		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef(0, 0.5F, 0);
		
		GL11.glRotatef(((TileEntityMirror)tileentity).yaw, 0, 1F, 0);
		GL11.glRotatef(((TileEntityMirror)tileentity).pitch, 0, 0, 1F);

		GL11.glTranslatef(0, -0.5f, 0);

		model.renderOnly("Head_mirror");
		
		GL11.glPopMatrix();
				
		model.renderAllExcept("Head_mirror");

		GL11.glPopMatrix();

	}

}
