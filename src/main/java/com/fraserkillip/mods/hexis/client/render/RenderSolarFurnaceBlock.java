package com.fraserkillip.mods.hexis.client.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.IModelCustom;

import org.lwjgl.opengl.GL11;

import com.fraserkillip.mods.hexis.tileentities.TileEntitySolarFurnace;

public class RenderSolarFurnaceBlock extends TileEntitySpecialRenderer {

	private IModelCustom model;

	public RenderSolarFurnaceBlock(IModelCustom model) {
		this.model = model;
	}

	public static final ResourceLocation texture = new ResourceLocation("hexis", "textures/models/solar_furnace_block.png");

	@Override
	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f) {		
		GL11.glPushMatrix();
	
		GL11.glTranslatef((float) x + 0.5F, (float) y, (float) z + 0.5F);
		GL11.glScalef(1.1F, 1.1F, 1.1F);

		Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
		
		model.renderAllExcept("Head_head");
		
		GL11.glPushMatrix();
		
		GL11.glTranslatef(0, 0.35F, 0);
		
		GL11.glRotatef(((TileEntitySolarFurnace)tileentity).pitch, 0, 0, 1F);

		GL11.glTranslatef(0, -0.35f, 0);

		model.renderOnly("Head_head");
				
		GL11.glPopMatrix();		
		GL11.glPopMatrix();

	}

}
