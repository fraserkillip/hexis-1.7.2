package com.fraserkillip.mods.hexis.items;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class HexisItems {
	
	public static Item clipboard;
	public static Item graviStaff;
	
	// Corundum
	public static Item corundumRaw;
	public static Item corundumMono;
	public static Item corundumPure;
	public static Item corundumTi;
	public static Item corundumTiBlue;
	public static Item corundumBlue;
	public static Item corundumRed;
	
	// Optic
	public static Item lensConverging;
	public static Item lensDiverging;
	public static Item lensMount;
	public static Item lensGuide;
	public static Item lensFocus;
	public static Item lensSizer;
	
	// Tools
	public static Item opTechsTool;
	
	public static void init() {
		clipboard = new ItemClipboard();
		graviStaff = new ItemGraviStaff();
		
		// ##### Corundum
		corundumRaw = new ItemCorundumRaw();
		corundumMono = new ItemCorundumMono();
		corundumPure = new ItemCorundumPure();
		corundumTi = new ItemCorundumTi();
		corundumTiBlue = new ItemCorundumTiBlue();
		corundumRed = new ItemCorundumRed();
		corundumBlue = new ItemCorundumBlue();
		
        GameRegistry.registerItem(clipboard, "item." + ItemInfo.CLIPBOARD_UNLOCALIZED);
        GameRegistry.registerItem(graviStaff, "item." + ItemInfo.GRAVISTAFF_UNLOCALIZED);
        GameRegistry.registerItem(corundumRaw, "item." + ItemInfo.CORUNDUM_RAW_UNLOCALIZED);
        GameRegistry.registerItem(corundumMono, "item." + ItemInfo.CORUNDUM_MONO_UNLOCALIZED);
        GameRegistry.registerItem(corundumPure, "item." + ItemInfo.CORUNDUM_PURE_UNLOCALIZED);
        GameRegistry.registerItem(corundumTi, "item." + ItemInfo.CORUNDUM_TI_UNLOCALIZED);
        GameRegistry.registerItem(corundumTiBlue, "item." + ItemInfo.CORUNDUM_TI_BLUE_UNLOCALIZED);
        GameRegistry.registerItem(corundumRed, "item." + ItemInfo.CORUNDUM_RED_UNLOCALIZED);
        GameRegistry.registerItem(corundumBlue, "item." + ItemInfo.CORUNDUM_BLUE_UNLOCALIZED);        
        
		// ##### Optic
		lensConverging = new ItemLensConverging();
		lensDiverging = new ItemLensDiverging();
		lensMount = new ItemLensMount();
		lensGuide = new ItemLensGuide();
		lensFocus = new ItemLensFocus();
		lensSizer = new ItemLensSizer();
        
        GameRegistry.registerItem(lensConverging, "item." + ItemInfo.LENS_CONVERGING_UNLOCALIZED);
        GameRegistry.registerItem(lensDiverging, "item." + ItemInfo.LENS_DIVERGING_UNLOCALIZED);
        GameRegistry.registerItem(lensMount, "item." + ItemInfo.LENS_MOUNT_UNLOCALIZED);
        GameRegistry.registerItem(lensGuide, "item." + ItemInfo.LENS_GUIDE_UNLOCALIZED);
        GameRegistry.registerItem(lensFocus, "item." + ItemInfo.LENS_FOCUS_UNLOCALIZED);
        GameRegistry.registerItem(lensSizer, "item." + ItemInfo.LENS_SIZER_UNLOCALIZED);
        
		// ##### Tools
        opTechsTool = new ItemOpTechsTool();
        GameRegistry.registerItem(opTechsTool, "item." + ItemInfo.OPTECHS_MULTITOOL_UNLOCALIZED);

        
	}
	
	public static void registerRecipes() {
		GameRegistry.addRecipe(new ItemStack(clipboard),
				new Object[] { 	" I ",
								"WPW",
								"WPW",
								
								'I', Items.iron_ingot,
								'P', Items.paper,
								'W', Blocks.planks
							 });
		
		GameRegistry.addRecipe(new ItemStack(lensFocus),
				new Object[] {	"GMC",
								
								'G', lensGuide,
								'M', lensMount,
								'C', lensConverging
							 });
		
		GameRegistry.addRecipe(new ItemStack(lensSizer),
				new Object[] {	"C D",
								"MGM",
								
								'G', lensGuide,
								'M', lensMount,
								'C', lensConverging,
								'D', lensDiverging
							 });
		
		GameRegistry.addRecipe(new ItemStack(lensSizer),
				new Object[] {	"D C",
								"MGM",
								
								'G', lensGuide,
								'M', lensMount,
								'C', lensConverging,
								'D', lensDiverging
							 });
		
		GameRegistry.addRecipe(new ItemStack(lensSizer),
				new Object[] {	"DMF",
								
								'F', lensFocus,
								'M', lensMount,
								'D', lensDiverging
							 });
	}
	
}
