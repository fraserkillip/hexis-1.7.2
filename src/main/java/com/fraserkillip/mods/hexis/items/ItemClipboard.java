package com.fraserkillip.mods.hexis.items;

import com.fraserkillip.mods.hexis.Hexis;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemClipboard extends Item {

	public ItemClipboard() {
		setMaxStackSize(1);
		setUnlocalizedName(ItemInfo.CLIPBOARD_UNLOCALIZED);
		setCreativeTab(Hexis.creativeTab);
		setMaxDamage(100);
	}
}
