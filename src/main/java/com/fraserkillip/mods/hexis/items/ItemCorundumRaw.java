package com.fraserkillip.mods.hexis.items;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

import com.fraserkillip.mods.hexis.Hexis;
import com.fraserkillip.mods.hexis.ModInformation;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemCorundumRaw extends Item{
	
	public ItemCorundumRaw() {
		setUnlocalizedName(ItemInfo.CORUNDUM_RAW_UNLOCALIZED);
		setCreativeTab(Hexis.creativeTab);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister register)
    {
        itemIcon = register.registerIcon(ModInformation.TEXTURE_LOC + ":" + ItemInfo.CORUNDUM_RAW_ICON);
    }
}
