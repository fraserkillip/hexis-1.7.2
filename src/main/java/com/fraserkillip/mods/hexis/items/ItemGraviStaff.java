package com.fraserkillip.mods.hexis.items;

import com.fraserkillip.mods.hexis.Hexis;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemGraviStaff extends Item{

	private int maxDamage;
	private int tickDelay = 1;
	
	public ItemGraviStaff() {
		setMaxStackSize(1);
		setUnlocalizedName(ItemInfo.GRAVISTAFF_UNLOCALIZED);
		setCreativeTab(Hexis.creativeTab);
		maxDamage = ItemInfo.GRAVISTAFF_COOLDOWN * 20;
		setMaxDamage(maxDamage);
	}

	@Override
    public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5)
    {
		if(stack.getItemDamage() != 0 && !world.isRemote && ((tickDelay++) % 5 == 0)) {
			stack.setItemDamage(stack.getItemDamage() - 5);
			tickDelay = 1;
		}
    }
	
	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
		if(stack.getItemDamage() == 0  && !entity.worldObj.isRemote) {
			entity.motionY += 5;
			stack.setItemDamage(maxDamage);
		}
		return true;
		
	}
	
}
