package com.fraserkillip.mods.hexis.items;

public class ItemInfo {
	
	public static final String CLIPBOARD_UNLOCALIZED = "clipboard";
	
	public static final String GRAVISTAFF_UNLOCALIZED = "gravistaff";
	public static int GRAVISTAFF_COOLDOWN;
	public static final String GRAVISTAFF_COOLDOWN_KEY = "GraviStaffCooldown";
	public static final int GRAVISTAFF_COOLDOWN_DEFAULT = 20;
	
	
	/*************************************************************************
	 * Corundum
	 */
	public static final String CORUNDUM_RAW_UNLOCALIZED = "corundumRaw";
	public static final String CORUNDUM_RAW_ICON = "corundum_raw";
	
	public static final String CORUNDUM_MONO_UNLOCALIZED = "corundumMono";
	public static final String CORUNDUM_MONO_ICON = "corundum_mono";
	
	public static final String CORUNDUM_PURE_UNLOCALIZED = "corundumPure";
	public static final String CORUNDUM_PURE_ICON = "corundum_pure";
	
	public static final String CORUNDUM_TI_UNLOCALIZED = "corundumTi";
	public static final String CORUNDUM_TI_ICON = "corundum_ti";
	
	public static final String CORUNDUM_TI_BLUE_UNLOCALIZED = "corundumTiBlue";
	public static final String CORUNDUM_TI_BLUE_ICON = "corundum_ti_blue";
	
	public static final String CORUNDUM_BLUE_UNLOCALIZED = "corundumBlue";
	public static final String CORUNDUM_BLUE_ICON = "corundum_blue";
	
	public static final String CORUNDUM_RED_UNLOCALIZED = "corundumRed";
	public static final String CORUNDUM_RED_ICON = "corundum_red";
	
	/*************************************************************************
	 * Optics
	 */
	public static final String LENS_CONVERGING_UNLOCALIZED = "lensConverging";
	public static final String LENS_CONVERGING_ICON = "lens_converging";
	
	public static final String LENS_DIVERGING_UNLOCALIZED = "lensDiverging";
	public static final String LENS_DIVERGING_ICON = "lens_diverging";
	
	public static final String LENS_MOUNT_UNLOCALIZED = "lensMount";
	public static final String LENS_MOUNT_ICON = "lens_mount";
	
	public static final String LENS_GUIDE_UNLOCALIZED = "lensGuide";
	public static final String LENS_GUIDE_ICON = "lens_guide";
	
	public static final String LENS_FOCUS_UNLOCALIZED = "lensFocus";
	public static final String LENS_FOCUS_ICON = "lens_focus";
	
	public static final String LENS_SIZER_UNLOCALIZED = "lensSizer";
	public static final String LENS_SIZER_ICON = "lens_sizer";
	
	/*************************************************************************
	 * Tools
	 */
	public static final String OPTECHS_MULTITOOL_UNLOCALIZED = "opTechsTool";
	public static final String OPTECHS_MULTITOOL_ICON = "op-techs_multitool";
}
