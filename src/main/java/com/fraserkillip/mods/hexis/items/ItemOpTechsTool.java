package com.fraserkillip.mods.hexis.items;

import java.util.List;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import com.fraserkillip.mods.hexis.Hexis;
import com.fraserkillip.mods.hexis.ModInformation;
import com.ibm.icu.impl.ICUService.Key;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemOpTechsTool extends Item {

	public ItemOpTechsTool() {
		setMaxStackSize(1);
		setUnlocalizedName(ItemInfo.OPTECHS_MULTITOOL_UNLOCALIZED);
		setCreativeTab(Hexis.creativeTab);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		itemIcon = register.registerIcon(ModInformation.TEXTURE_LOC + ":" + ItemInfo.OPTECHS_MULTITOOL_ICON);
	}

	@Override
	public boolean onItemUseFirst(ItemStack itemStack, EntityPlayer player, World par3World, int x, int y, int z, int par7, float par8, float par9, float par10) {
		System.out.println(player.isSneaking());
		itemStack.stackTagCompound = new NBTTagCompound();
		itemStack.stackTagCompound.setInteger("x", x);
		itemStack.stackTagCompound.setInteger("y", y);
		itemStack.stackTagCompound.setInteger("z", z);
		return true;
	}

	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4) {
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT)) {
			if (itemStack.stackTagCompound != null && itemStack.stackTagCompound.hasKey("x")) {
				list.add(EnumChatFormatting.GRAY + "Currently bound to block at:");
				list.add(EnumChatFormatting.RED + "X: " + itemStack.stackTagCompound.getInteger("x"));
				list.add(EnumChatFormatting.GREEN + "Y: " + itemStack.stackTagCompound.getInteger("y"));
				list.add(EnumChatFormatting.BLUE + "Z: " + itemStack.stackTagCompound.getInteger("z"));
			} else {
				list.add(EnumChatFormatting.GRAY + "No bound block");
			}
		} else {
			list.add(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "Hold shift for details");
		}
	}
}
