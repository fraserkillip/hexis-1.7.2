package com.fraserkillip.mods.hexis.proxies;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;

import com.fraserkillip.mods.hexis.client.render.RenderMirrorBlock;
import com.fraserkillip.mods.hexis.client.render.RenderSolarFurnaceBlock;
import com.fraserkillip.mods.hexis.tileentities.TileEntityMirror;
import com.fraserkillip.mods.hexis.tileentities.TileEntitySolarFurnace;

import cpw.mods.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy {
	@Override
	public void initSounds() {
//		new SoundHandler();
	}

	@Override
	public void initRenderers() {
//		RenderingRegistry.registerEntityRenderingHandler(EntitySpaceship.class, new RenderSpaceship());
//		
//		ModelDroid model = new ModelDroid();
//		RenderingRegistry.registerEntityRenderingHandler(EntityDroid.class, new RenderDroid(model));
//		MinecraftForgeClient.registerItemRenderer(ItemInfo.DROID_ID + 256, new RenderDroidItem(model));
//		
//		RenderMachine machineRender = new RenderMachine();
//		BlockInfo.MACHINE_RENDER_ID = machineRender.getRenderId();
//		RenderingRegistry.registerBlockHandler(machineRender);
//		
//		RenderingRegistry.registerEntityRenderingHandler(EntityBomb.class, new RenderBomb());
		
		IModelCustom modelMirror = AdvancedModelLoader.loadModel(new ResourceLocation("hexis","/models/mirror.obj"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMirror.class, new RenderMirrorBlock(modelMirror));
//		MinecraftForgeClient.registerItemRenderer(BlockInfo.BOMB_ID, new RenderBombBlockItem(modelBomb));
		
		
		IModelCustom modelSolarFurnace = AdvancedModelLoader.loadModel(new ResourceLocation("hexis","/models/solar_furnace.obj"));
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySolarFurnace.class, new RenderSolarFurnaceBlock(modelSolarFurnace));
//		MinecraftForgeClient.registerItemRenderer(BlockInfo.BOMB_ID, new RenderBombBlockItem(modelBomb));
	}	
}
