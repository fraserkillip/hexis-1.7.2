package com.fraserkillip.mods.hexis.tileentities;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityMirror extends TileEntity {
	
	public float pitch = 0;
	private float targetPitch = 0;
	public float yaw = 0;
	private float targetYaw = 0;
	
	private int delay = 100;
	
	@Override
	public void updateEntity() {
		super.updateEntity();
		
		if(delay-- == 0) {
			delay = 100;
			
			targetPitch = (float) (Math.random()*90);
			targetYaw = (float) (Math.random()*360);
		}
		
		pitch += (targetPitch - pitch) * 0.05F;
		yaw += (targetYaw - yaw) * 0.05F;
	}
}
